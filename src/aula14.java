public class aula14 {

    public static void main(String[] args) {

        System.out.println( 3%2 );  // imprime resto da divisão 1
        System.out.println( 4%2 ); // imprime resto da divisão 0
        System.out.println( 5%2 ); // imprime resto da divisão 1
        System.out.println( 6%2 ); // imprime resto da divisão 0
        System.out.println( 300%100 ); // imprime resto da divisão 0
        System.out.println( 1300%100 ); // imprime resto da divisão 0
        System.out.println( 1301%100 ); // imprime resto da divisão 1

    }
}
